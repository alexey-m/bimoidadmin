<?php
/**
 * @license BimoidAdmin
 * (c) 2015 alexey-m, http://alexey-m.ru
 * License: MIT
 */

namespace bimoid;


class BimoidAdmin {
    // Windows server administration
    // BEX type
    const OBIMP_BEX_WADM = 0xF001;
    // BEX subtype
    const OBIMP_BEX_WADM_CLI_LOGIN = 0x0001;
    const OBIMP_BEX_WADM_SRV_LOGIN_REPLY = 0x0002;
    const OBIMP_BEX_WADM_CLI_PARAMS = 0x0003;
    const OBIMP_BEX_WADM_SRV_PARAMS_REPLY = 0x0004;
    const OBIMP_BEX_WADM_CLI_SET = 0x0005;
    const OBIMP_BEX_WADM_SRV_SET_REPLY = 0x0006;
    const OBIMP_BEX_WADM_CLI_BROADCAST = 0x0007;
    const OBIMP_BEX_WADM_SRV_BROADCAST_REPLY = 0x0008;
    const OBIMP_BEX_WADM_CLI_USER = 0x0009;
    const OBIMP_BEX_WADM_SRV_USER_REPLY =0x000a;
    const OBIMP_BEX_WADM_SRV_STATE = 0x000b;
    const OBIMP_BEX_WADM_CLI_LIST = 0x000c;
    const OBIMP_BEX_WADM_SRV_LIST_REPLY = 0x000d;
    const OBIMP_BEX_WADM_CLI_EXT_LIST_REQ = 0x000e;
    const OBIMP_BEX_WADM_SRV_EXT_LIST_REPLY = 0x000f;
    const OBIMP_BEX_WADM_CLI_EXT_UPD = 0x0010;
    const OBIMP_BEX_WADM_SRV_EXT_UPD_REPLY = 0x0011;

    // Login result codes:
    const ADM_RES_CODE_SUCCESS = 0x0000;
    const ADM_RES_CODE_BAD_KEY = 0x0001;

    // Broadcast flags:
    const BROADCAST_MSG_TO_ALL = 0x0001;
    const BROADCAST_MSG_TO_CONNECTED_ONLY = 0x0002;
    const BROADCAST_DROP_CONNECTIONS = 0x0003;

    // Broadcast result codes:
    const BC_RES_SUCCESS = 0x0000;
    const BC_RES_BAD_REQUEST = 0x0001;
    const BC_RES_MSG_LEN_LIMIT = 0x0002;

    // Setting change result codes:
    const SET_CHANGE_SUCCESS = 0x0000;
    const SET_CHANGE_NOT_ALLOWED = 0x0001;

    // Available extension types:
    const EXT_TYPE_TRANSPORT = 0x0001;

    // Available extensions proxy types
    const EXT_PROXY_TYPE_NONE = 0x00;
    const EXT_PROXY_TYPE_HTTP = 0x01;
    const EXT_PROXY_TYPE_SOCKS4 = 0x02;
    const EXT_PROXY_TYPE_SOCKS4A = 0x03;
    const EXT_PROXY_TYPE_SOCKS5 = 0x04;

    public $major = 0;
    public $minor = 0;
    public $release = 0;
    public $build = 0;

    public $server = '';
    /**
     * 7023 - TCP port number for client connections
     * 7024 - TCP port number for administrative connections
     * 7025 - TCP port number for file transfer direct/proxy client connections

     * 7033 - TCP port number for secure client connections over TLS/SSL
     * 7034 - TCP port number for secure administrative connections over TLS/SSL
     * 7035 - TCP port number for secure file transfer direct/proxy client connections over TLS/SSL
     */
    public $port = 7024;
    public $server_key = '';


    private $socket;
    private $sequence = 0;
    private $logined = false;


    private function connect(){

        /** connect to bimoid server */
        $address = gethostbyname($this->server);

        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if ($this->socket === false)
            return false;

        return socket_connect($this->socket, $address, $this->port);
    }

    /**
     * @param string $msg
     */
    private function sendMsg($msg){

        if (is_resource($this->socket)){

            socket_write($this->socket, $msg, strlen($msg));
        }
    }

    /**
     * @param int $size
     * @return string
     */
    private function recvMsg($size){

        $buffer = '';

        if (is_resource($this->socket)){

            socket_set_option($this->socket, SOL_SOCKET, SO_RCVTIMEO, ["sec" => 10, "usec" => 0]);

            while($size){
                if (false === ($recv = socket_read($this->socket, $size)) ) break;
                $buffer.= $recv;
                $size-= strlen($recv);
            }
        }
        return $buffer;
    }

    /**
     * @return array|bool
     */
    private function getPack(){

        $header = $this->recvMsg(17);

        /** Разбор заголовка пакета cNn2N2 */
        $packet = unpack('Csign/Nsequence/ntype/nsubtype/Nreq/Nsize', $header);

        /** Проверяем тип и сигнатуру пакета */
        if ($packet['sign'] === 0x23 && $packet['type'] === self::OBIMP_BEX_WADM){

            $packet['data'] = $this->recvMsg($packet['size']);
            return $packet;
        }
        return false;
    }

    /**
     * @param string $data
     * @param int $skip
     * @return array|bool
     */
    private function getWTLData(&$data, $skip = 0){

        if(!$data) return false;

        if($skip > 0) $data = substr($data, $skip);

        $result = unpack('Ntype/Nsize', substr($data, 0, 8));
        $result['data'] = substr($data, 8, $result['size']);
        $data = substr($data, 8+$result['size']);
        return $result;
    }

    /**
     *  OBIMP header structure (header is always 17 bytes length):
     *  - Byte: every server/client command starts from byte 0x23 - "#";
     *  - LongWord: sequence, from 0 to 0xFFFFFFFF, first packet has to start always from 0;
     *  - Word: BEX type
     *
     * @param integer $subtype  - Word: BEX subtype
     *  - LongWord: request id, used for marking client requests to link it with server replies
     *  - LongWord: following data length
     * @param string $data
     *
     * @return string
     */
    private function wTLDFinally($subtype, $data){
        return pack("cNn2N2a*", 0x23, $this->sequence++ , self::OBIMP_BEX_WADM, $subtype, 0, strlen($data), $data);
    }

    /**
     * wTLD structure is:
     * - Type: LongWord
     * - Length: LongWord
     * - Data: data with specified length
     *
     * @param int $type type wTLD
     * @param int|string $value
     * @param int|bool|false $length (Byte, Word, LongWord, QuadWord or false if string)
     * @return string binary wTLD packet data
     */
    private function wTLDPack($type, $value, $length = false){

        switch ($length) {
            case 1: // Byte
                $format = 'N2C';
                break;
            case 2: // Word
                $format = 'N2n';
                break;
            case 4: // LongWord
                $format = 'N3';
                break;
            case 8: // QuadWord
                $format = 'N2J';
                break;
            default:
                $format = 'N2a*';
                break;
        }
        if ($length === false) {
            $length = strlen($value);
        }
        return pack($format, $type, $length, $value);
    }

    /**
     * @param string $data
     * @param int $skip
     * @return array|bool
     */
    private function getSTLData(&$data, $skip = 0){

        if(!$data) return false;

        if($skip > 0) $data = substr($data, $skip);

        $result = unpack('ntype/nsize', substr($data, 0, 4));
        $result['data'] = substr($data, 4, $result['size']);
        $data = substr($data, 4+$result['size']);
        return $result;
    }

    /**
     * sTLD structure is:
     * - Type: Word
     * - Length: Word
     * - Data: data with specified length
     *
     * @param integer $type
     * @param integer|string $value
     * @param int|bool|false $length
     * @return string
     */
    private function sTLDPack($type, $value, $length = false){

        switch ($length) {
            case 1: // Byte
                $format = 'n2C';
                break;
            case 2: // Word
                $format = 'n3';
                break;
            case 4: // LongWord
                $format = 'n2N';
                break;
            case 8: // QuadWord
                $format = 'n2J';
                break;
            default:
                $format = 'n2a*';
                break;
        }
        if ($length === false) {
            $length = strlen($value);
        }
        return pack($format, $type, $length, $value);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function login(){

        $loginPack = $this->wTLDPack(1, $this->server_key);

        $msg = $this->wTLDFinally(self::OBIMP_BEX_WADM_CLI_LOGIN, $loginPack);

        if ($this->connect()){

            $this->sendMsg($msg);

            if ($srv = $this->getPack()){

                /** Проверяем субтип пакета */
                if ($srv['subtype'] == self::OBIMP_BEX_WADM_SRV_LOGIN_REPLY){

                    while ($wTLDs = $this->getWTLData($srv['data'])){

                        switch ($wTLDs['type']) {
                            case 1:
                                $rec = $this->TLD2Int($wTLDs['data']);

                                if ($rec == self::ADM_RES_CODE_SUCCESS){

                                    $this->logined = true;

                                } elseif ($rec == self::ADM_RES_CODE_BAD_KEY){

                                    socket_close($this->socket);
                                    throw new \Exception('ADM_RES_CODE_BAD_KEY', self::ADM_RES_CODE_BAD_KEY);
                                }
                                break;
                            case 2:
                                $srvVer = unpack('nmajor/nminor/nrelease/nbuild', $wTLDs['data']);
                                $this->major = $srvVer['major'];
                                $this->minor = $srvVer['minor'];
                                $this->release = $srvVer['release'];
                                $this->build = $srvVer['build'];
                                break;
                        }
                    }

                    return $this->logined;
                }
            };
        }
        return false;
    }

    /**
     * @param string $value
     * @return integer
     */
    private function TLD2Int($value){

        switch (strlen($value)){
            case 1:
            default:
                $format = 'C';
                break;
            case 2:
                $format = 'n';
                break;
            case 4:
                $format = 'N';
                break;
            case 8:
                $format = 'J';
                break;
        }
        $result =  unpack($format.'value', $value);
        return $result['value'];
    }

    /**
     * @param string $value
     * @return bool
     */
    private function TLD2Bool($value){
        return (bool)($value === chr(0));
    }

    /**
     * @return array|bool
     */
    public function getSrvParams(){

        $msg = $this->wTLDFinally(self::OBIMP_BEX_WADM_CLI_PARAMS, '');
        $this->sendMsg($msg);

        if ($recv = $this->getPack()){

            /** Проверяем тип и субтип пакета */
            if ($recv['subtype'] == self::OBIMP_BEX_WADM_SRV_PARAMS_REPLY){

                $params = [];

                while ($wTLD = $this->getWTLData($recv['data'])){

                    switch ($wTLD['type']) {
                        // BLK
                        case 45:
                        $params[$wTLD['type']] = $wTLD['data'];
                        break;
                        // string
                        case 1: case 29: case 31: case 32: case 33:
                        case 34: case 36: case 37: case 40: case 48:
                        case 70: case 72: case 73: case 74:
                        $params[$wTLD['type']] = $wTLD['data'];
                        break;
                        // Bool
                        case 4: case 5: case 6: case 7:
                        case 8: case 30: case 38: case 39:
                        case 41: case 42: case 46: case 47:
                        case 51: case 53: case 54: case 55:
                        case 56: case 57: case 58: case 63:
                        case 67: case 68: case 75: case 76:
                        case 77:
                        $params[$wTLD['type']] = $this->TLD2Bool($wTLD['data']);
                        break;
                        // Integer
                        case 2: case 3:  case 9: case 10: case 11: case 12:
                        case 13: case 14: case 15: case 16: case 17:
                        case 18: case 19: case 20: case 21: case 22:
                        case 23: case 24:  case 25: case 26: case 27:
                        case 28: case 35: case 43: case 44: case 49:
                        case 50: case 52: case 59: case 60: case 61:
                        case 62: case 64: case 65: case 66: case 69:
                        case 71:
                        $params[$wTLD['type']] = $this->TLD2Int($wTLD['data']);
                        break;
                    }
                }
                return $params;
            }
        }

        return false;
    }

    /**
     * Subtype: 0x0005. (OBIMP_BEX_WADM_CLI_SET)
     * To set server parameters client has to send this BEX. All wTLDs are the same like
     * in BEX OBIMP_BEX_WADM_SRV_PARAMS_REPLY. Client should send only
     * changed settings wTLDs.
     *
     * @param array $params
     * @return array|bool       return array setting change status otherwise FALSE
     */
    public function setSrvParams(array $params){

        /**
         * Client must not send following wTLDs:
         *  wTLD 0x0004: Bool, start main server on system start (deprecated)
         *  wTLD 0x0005: Bool, start administration server on system start (deprecated)
         */
        unset($params[4]);
        unset($params[5]);
        /**
         * Client must not send following wTLDs:
         *  wTLD 0x000B: LongWord, current connected clients count
         *  wTLD 0x000C: LongWord, total registered users count
         */
        unset($params[11]);
        unset($params[12]);

        $setParams = '';

        foreach ($params as $key => $param){

            // if param is int (LongWord)
            if ( in_array($key,[2,3,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,35,43,44,49,50,52,59,60,61,62,64,65,66,71]) ){
                $setParams .=  $this->wTLDPack($key, $param, 4);
            }
            // if param is bool
            if ( in_array($key,[4,5,6,7,8,30,38,39,41,42,46,47,51,53,54,55,56,57,58,63,67,68,69,75,76,77]) ){
                $setParams .=  $this->wTLDPack($key, $param, 1);
            }
            // if param is string
            if ( in_array($key,[1,29,31,32,33,34,36,37,40,48,45,70,72,73,74]) ){
                $setParams .=  $this->wTLDPack($key, $param);
            }
        }

        $msgPack = $this->wTLDFinally(self::OBIMP_BEX_WADM_CLI_SET, $setParams);

        $this->sendMsg($msgPack);

        if ($recv = $this->getPack()){

            if ($recv['subtype'] == self::OBIMP_BEX_WADM_SRV_SET_REPLY){

                $result = [];

                while ($wTLD = $this->getWTLData($recv['data'])){

                    $result[$wTLD['type']] = $this->TLD2Int($wTLD['data']) == self::SET_CHANGE_SUCCESS ? true : false;
                }
                return $result;
            }
        }
        return false;
    }

    /**
     * Subtype: 0x0007. (OBIMP_BEX_WADM_CLI_BROADCAST)
     * Client is trying to send system message to clients or to disconnect all clients.
     * The sender of system message will be OBIMP packet begin sign "#"
     * and all such messages will be marked using system message flag.
     *
     * @param int $type         BROADCAST_MSG_TO_ALL, BROADCAST_MSG_TO_CONNECTED_ONLY, BROADCAST_DROP_CONNECTIONS
     * @param string $message   system message text (maximal UTF-8 encoded length is 2048)
     * @param int $position     system message popup position (0 - default, 1 - screen center)
     *
     * @return bool|int         BC_RES_SUCCESS, BC_RES_BAD_REQUEST, BC_RES_MSG_LEN_LIMIT otherwise FALSE
     */
    public function sendBroadcastMsg($type, $message, $position = 0){

        $position = $position === 1 ? 1 : 0;

        if (strlen($message) > 2048) {

            $message = substr($message, 0, 2048);
        }

        $broadcast = $this->wTLDPack(1, $type, 2);

        if ($type !== self::BROADCAST_DROP_CONNECTIONS){
            $broadcast .= $this->wTLDPack(2, $message);
            $broadcast .= $this->wTLDPack(3, $position, 1);
        }
        $msgPack = $this->wTLDFinally(self::OBIMP_BEX_WADM_CLI_BROADCAST, $broadcast);

        if ($this->logined){

            $this->sendMsg($msgPack);

            if ($recv = $this->getPack()){

                /** Проверяем субтип пакета */
                if ($recv['subtype'] == self::OBIMP_BEX_WADM_SRV_BROADCAST_REPLY){

                    $wTLD = unpack('Ntype/Nsize/ncode', $recv['data']);

                    return (int)$wTLD['code'];

                }
            }

        }

        return false;
    }

    /**
     * Subtype: 0x000E. (OBIMP_BEX_WADM_CLI_EXT_LIST_REQ)
     * Client is trying to get list of available extensions of specified type.
     *
     * @return array|bool   return array extensions otherwise FALSE
     */
    public function getExtList(){

        $extTypes = $this->wTLDPack(1, self::EXT_TYPE_TRANSPORT, 2);
        $msgPack = $this->wTLDFinally(self::OBIMP_BEX_WADM_CLI_EXT_LIST_REQ, $extTypes);

        if ($this->logined){

            $this->sendMsg($msgPack);

            if ($recv = $this->getPack()){

                if ($recv['subtype'] == self::OBIMP_BEX_WADM_SRV_EXT_LIST_REPLY){

                    /** Получаем тип расширений */
                    $wTLD = $this->getWTLData($recv['data']);
                    $extType = $this->TLD2Int($wTLD['data']);

                    /** Если это транспорты, то парсим элементы */
                    if ($extType == self::EXT_TYPE_TRANSPORT){

                        /**
                         * Структура элементов транспорта:
                         *  LongWord - число элементов
                         *  BLK - массив элементов
                         */

                        $wTLD = $this->getWTLData($recv['data']);

                        $extCount = $this->TLD2Int(substr($wTLD['data'], 0, 4));
                        $extBin = substr($wTLD['data'], 4);

                        /**
                         * Массив состоит из:
                         * UUID - 16 байтов
                         * LongWord - длина sTLDs блоков
                         * array sTLD blocks
                         */

                        $extensions = [];
                        $pos = 0;

                        while ($extCount){

                            $extension['uuid'] = bin2hex(substr($extBin,$pos, 16));
                            $sizeTLD = $this->TLD2Int(substr($extBin, $pos + 16, 4));
                            $sTLDs = substr($extBin, $pos + 20, $sizeTLD);
                            $pos += $sizeTLD + 20;

                            while ($sTLD = $this->getSTLData($sTLDs)){

                                switch ($sTLD['type']){
                                    case 1:
                                        $extension['name'] = $sTLD['data'];
                                        break;
                                    case 2:
                                        $extension['version'] = $sTLD['data'];
                                        break;
                                    case 3:
                                        $extension['author'] = $sTLD['data'];
                                        break;
                                    case 4:
                                        $extension['url'] = $sTLD['data'];
                                        break;
                                    case 5:
                                        $extension['objCount'] = $this->TLD2Int($sTLD['data']);
                                        break;
                                    case 6:
                                        $extension['enabled'] = $this->TLD2Bool($sTLD['data']);
                                        break;
                                    case 7:
                                        $extension['useUserOnly'] = $this->TLD2Bool($sTLD['data']);
                                        break;
                                    case 8:
                                        $extension['accounts'] = $sTLD['data'];
                                        break;
                                    case 160:
                                        $extension['proxy']['type'] = $this->TLD2Int($sTLD['data']);
                                        break;
                                    case 161:
                                        $extension['proxy']['host'] = $sTLD['data'];
                                        break;
                                    case 162:
                                        $extension['proxy']['port'] = $this->TLD2Int($sTLD['data']);
                                        break;
                                    case 163:
                                        $extension['proxy']['user'] = $sTLD['data'];
                                        break;
                                    case 164:
                                        $extension['proxy']['pwd'] = (string)$sTLD['data'];
                                        break;
                                }
                            }

                            $extensions[] = $extension;
                            $extCount--;
                        }

                        return $extensions;
                    }
                }
            }
        }
        return false;
    }




}